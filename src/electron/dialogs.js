const { BrowserWindow } = require('electron');
const path = require("path");

let currentInfoDialog;
const openInfoDialog = function () {
    if (currentInfoDialog) {
        currentInfoDialog.focus();
        return;
    }

    const parentBounds = BrowserWindow.getFocusedWindow().getBounds();
    const middleX = parentBounds.x + (parentBounds.width / 2);
    const middleY = parentBounds.y + (parentBounds.height / 2);

    const width = 450;
    const height = 220;

    currentInfoDialog = new BrowserWindow({
        width: width,
        height: height,
        x: middleX - (width / 2),
        y: middleY - (height / 2),
        fullscreenable: false,
        resizable: false,
        minimizable: false,
        titleBarStyle: 'hidden',
        titleBarOverlay: {
            color: '#006e8b',
            symbolColor: '#000000'
        },
        show: false,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            preload: path.join(__dirname, 'preload.js'),
        }
    });
    currentInfoDialog.once('ready-to-show', () => {
        currentInfoDialog.show()
    });

    currentInfoDialog.on('closed', () => {
        currentInfoDialog = null;
    });

    require('@electron/remote/main').enable(currentInfoDialog.webContents);
    currentInfoDialog.loadFile(path.join(__dirname, 'pages/info.html')).then();
}

module.exports = {
    openInfoDialog
}
