const { app, BrowserWindow, ipcMain} = require('electron');
const path = require("path");
const dialogs = require('./dialogs.js');

require('@electron/remote/main').initialize();

require('./appMenu.js');

let mainWindow;

const createWindow = () => {
    mainWindow = new BrowserWindow({
        width: 500,
        height: 790,
        // fullscreenable: false,
        // resizable: false,
        titleBarStyle: 'hidden',
        titleBarOverlay: {
            color: '#006e8b',
            symbolColor: '#000000'
        },
        acceptFirstMouse: true,
        show: false,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true,
            contextIsolation: false
        }
    });
    mainWindow.once('ready-to-show', () => {
        mainWindow.show()
    });

    mainWindow.on('closed', () => {
        mainWindow = null;
        app.quit();
    });

    require("@electron/remote/main").enable(mainWindow.webContents);

    mainWindow.loadFile(path.join(__dirname, 'pages/main.html')).then();
};

app.whenReady().then(() => {
    createWindow();

    app.on('window-all-closed', () => {
        // if (process.platform !== 'darwin') {
            app.quit();
        // }
    });

    app.on('activate', () => {
        if (!mainWindow) {
            createWindow();
        }
    });
});

ipcMain.on('open-dialog', (event, args) => {
    switch (args.dialog) {
        case 'info':
            dialogs.openInfoDialog();
            break;
        case 'preferences':
            dialogs.openSettingsDialog();
            break;
    }
});
