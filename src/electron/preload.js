const { app } = require('@electron/remote');

window.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.var-appName').forEach(e => e.innerText = app.name);
    document.querySelectorAll('.var-appVersion').forEach(e => e.innerText = app.getVersion());
});
