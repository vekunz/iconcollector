import fs from "fs/promises";

const osTypeMap = {
    "16x16": "icp4",
    "32x32": "icp5",
    "64x64": "icp6",
    "128x128": "ic07",
    "256x256": "ic08",
    "512x512": "ic09",
    "512x512@2x": "ic10",
    "16x16@2x": "ic11",
    "32x32@2x": "ic12",
    "128x128@2x": "ic13",
    "256x256@2x": "ic14"
}

export async function BuildIcns(files) {
    const fs = require('fs/promises');
    const { Icns, IcnsImage } = require('@fiahfy/icns');

    const icon = new Icns();

    for (const [size, filePath] of Object.entries(files)) {
        const buf = await fs.readFile(filePath);
        const image = IcnsImage.fromPNG(buf, osTypeMap[size]);
        icon.append(image);
    }

    return icon.data;
}

export async function BuildIco(files) {
    const fs = require('fs/promises');
    const { Ico, IcoImage } = require('@fiahfy/ico');

    const icon = new Ico();

    for (const [size, filePath] of Object.entries(files)) {
        const buf = await fs.readFile(filePath);
        const image = IcoImage.fromPNG(buf);
        icon.append(image);
    }

    return icon.data;
}
