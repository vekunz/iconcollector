const { dialog, getCurrentWindow } = require('@electron/remote');

import '@material/mwc-icon';

const html = document.createElement('template');
const style = document.createElement('template');

html.innerHTML = `
<div class="placeholder">
    <span class="error"></span>
    <span class="size"></span>
    <mwc-icon class="add-button">add_circle</mwc-icon>
    <img src="" alt="" class="image" />
    <mwc-icon class="remove-button">cancel</mwc-icon>
</div>
`;

style.innerHTML = `
<style>
    :host {
        display: block;
    }
    .placeholder {
        position: relative;
        display: flex;
        align-items: center;
        justify-content: center;
        
        width: 100%;
        height: 100%;
        border: 3px dashed lightgray;
        border-radius: 20%;
    }
    .error {
        color: red;
        text-align: center;
        font-size: 0.9em;
    }
    .size {
        color: lightgray;
        font-weight: bold;
        text-align: center;
        font-size: 0.9em;
    }
    .image {
        max-width: 90%;
        max-height: 90%;
    }
    .remove-button {
        cursor: pointer;
        position: absolute;
        top: 0;
        right: 0;
        color: gray;
    }
    .add-button {
        cursor: pointer;
        position: absolute;
        bottom: 5%;
        color: gray;
    }
</style>
`;

class Image extends HTMLElement {
    #compPlaceholder;
    #compSize;
    #compImage;
    #compRemoveButton;
    #compAddButton;
    #compError;

    #size;
    #resolution;
    #image;
    #error;

    constructor() {
        super();
        this.attachShadow({mode: 'open'});

        const template = html.content.cloneNode(true);
        this.shadowRoot.appendChild(template);

        if (CSSStyleSheet.prototype.replaceSync) {
            const stylesheet = new CSSStyleSheet();
            stylesheet.replaceSync(style.content.querySelector('style').innerText);
            this.shadowRoot.adoptedStyleSheets = [stylesheet];
        } else {
            this.shadowRoot.appendChild(style.content.cloneNode(true));
        }

        this.#compPlaceholder = this.shadowRoot.querySelector('.placeholder');
        this.#compSize = this.shadowRoot.querySelector('.size');
        this.#compImage = this.shadowRoot.querySelector('.image');
        this.#compRemoveButton = this.shadowRoot.querySelector('.remove-button');
        this.#compAddButton = this.shadowRoot.querySelector('.add-button');
        this.#compError = this.shadowRoot.querySelector('.error');

        this.#compRemoveButton.onclick = () => this.#removeImage();
        this.#compAddButton.onclick = () => this.#addImage();

        this.#updateComponent();
    }

    get error() {
        return this.#error;
    }

    get size() {
        return this.#size;
    }
    set size(value) {
        if (value) {
            this.setAttribute('size', value);
        } else {
            this.removeAttribute('size');
        }
    }

    get resolution() {
        return this.#resolution;
    }
    set resolution(value) {
        if (value) {
            this.setAttribute('resolution', value);
        } else {
            this.removeAttribute('resolution');
        }

    }

    get image() {
        return this.#image;
    }
    set image(value) {
        if (value) {
            this.setAttribute('image', value);
        } else {
            this.removeAttribute('image');
        }
    }

    static get observedAttributes() {
        return ['size', 'resolution', 'image'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'size':
                this.#size = parseInt(newValue);
                this.#updateComponent();
                break;
            case 'resolution':
                this.#resolution = parseInt(newValue);
                this.#updateComponent();
                break;
            case 'image':
                this.#image = newValue;
                this.#checkImage(newValue)
                    .then(() => {
                        this.#error = null;
                        if (oldValue !== newValue) {
                            this.dispatchEvent(new CustomEvent('change', {bubbles: true, cancelable: true, detail: {image: this.#image, valid: true}}));
                        }
                    })
                    .catch(error => {
                        this.#error = error.message;
                        this.dispatchEvent(new CustomEvent('change', {bubbles: true, cancelable: true, detail: {image: this.#image, valid: false}}));
                    })
                    .finally(() => this.#updateComponent());
                break;
        }
    }

    #updateComponent() {
        if (this.#error) {
            this.#compError.innerHTML = this.#error;
            this.#compSize.innerText = '';
            this.#compImage.src = '';
            this.#compRemoveButton.style.display = '';
            this.#compAddButton.style.display = '';
            this.#compPlaceholder.style.border = '';
        } else if (!this.#image && this.#size && this.#resolution) {
            this.#compSize.innerHTML = this.#sizeText();
            this.#compImage.src = '';
            this.#compRemoveButton.style.display = 'none';
            this.#compAddButton.style.display = '';
            this.#compPlaceholder.style.border = '';
            this.#compError.innerText = '';
        } else if (this.#image) {
            this.#compSize.innerText = '';
            this.#compImage.src = this.#image;
            this.#compRemoveButton.style.display = '';
            this.#compAddButton.style.display = 'none';
            this.#compPlaceholder.style.border = 'none';
            this.#compError.innerText = '';
        }
    }

    #sizeText() {
        const endSize = this.#size * this.#resolution;

        if (this.#resolution === 1) {
            return `${endSize}x${endSize}`;
        } else {
            return `${this.#size}x${this.#size}@${this.#resolution}x<br />(${endSize}x${endSize})`;
        }
    }

    #removeImage() {
        this.#error = null;
        this.image = null;
    }

    async #addImage() {
        const result = await dialog.showOpenDialog(getCurrentWindow(), {
            title: 'Select image',
            filters: [{
                name: 'PNG',
                extensions: ['png']
            }],
            properties: ['openFile']
        });

        if (!result.canceled) {
            this.image = result.filePaths[0];
        }
    }

    async #checkImage(imagePath) {
        if (!imagePath) {
            return;
        }

        const fs = require('fs/promises');
        const PNG = require("pngjs").PNG;

        const file = await fs.readFile(imagePath);

        const png = new PNG();

        try {
            await new Promise((resolve, reject) => {
                png.parse(file, (error, data) => {
                    if (error)
                        reject(error);
                    else
                        resolve(data);
                });
            });
        } catch (e) {
            throw Error('Invalid image');
        }

        const actualSize = this.#size * this.#resolution;
        if (png.width !== actualSize || png.height !== actualSize) {
            throw Error(`Invalid size:<br />${png.width}x${png.height}`);
        }
    }
}
window.customElements.define('ic-image', Image);
