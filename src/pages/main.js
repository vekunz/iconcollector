const { ipcRenderer } = require('electron');
const { dialog, getCurrentWindow } = require('@electron/remote');

import presets from '../presets.json';
import { BuildIcns, BuildIco } from "../modules/icon-builder.js";

import '../modules/image.component';
import '@material/mwc-button';
import '@material/mwc-icon-button';
import '@material/mwc-list/mwc-list-item';
import '@material/mwc-select';
import '@material/mwc-top-app-bar-fixed';

const infoButton = document.getElementById('infoButton');
const presetSelector = document.getElementById('preset-selector');
const imageGrid = document.getElementById('image-grid');
const importIconSetButton = document.getElementById('importIconset');
const fileTypeLabel = document.getElementById('fileType');
const exportButton = document.getElementById('exportButton');
const clearButton = document.getElementById('clearAll');

infoButton.onclick = openInfoDialog;
presetSelector.onchange = presetChanged;
importIconSetButton.onclick = importIconSet;
exportButton.onclick = exportIcon;
clearButton.onclick = clearView;

let imagePlaceholders = [];

(function() {
    for (const [key, preset] of Object.entries(presets)) {
        const listItem = document.createElement('mwc-list-item');
        listItem.value = key;
        listItem.innerText = preset.name;

        presetSelector.appendChild(listItem);
    }
})();

function presetChanged() {
    if (presetSelector.value) {
        applyPreset();
    } else {
        imagePlaceholders = [];
        imageGrid.innerHTML = '';
    }
    checkState();
}

function applyPreset() {
    imageGrid.innerHTML = '';
    imagePlaceholders = [];
    const preset = currentPreset();

    // table head
    let row = document.createElement('tr');
    let cell = document.createElement('th');
    cell.innerText = 'Resolution';
    row.appendChild(cell);
    for (const res of preset.resolutions) {
        const cell = document.createElement('th');
        cell.innerText = `${res}x`;
        row.appendChild(cell);
    }
    imageGrid.appendChild(row);

    for (const size of preset.sizes) {
        let row = document.createElement('tr');
        let cell = document.createElement('td');
        cell.innerText = `${size}x${size}`;
        row.appendChild(cell);

        for (const res of preset.resolutions) {
            let cell = document.createElement('td');
            let icImage = document.createElement('ic-image');
            icImage.size = size;
            icImage.resolution = res;
            icImage.addEventListener('change', () => checkState());
            imagePlaceholders.push(icImage);
            cell.appendChild(icImage);
            row.appendChild(cell);
        }
        imageGrid.appendChild(row);
    }

    fileTypeLabel.innerText = preset.output.format;
}

async function importIconSet() {
    const result = await dialog.showOpenDialog(getCurrentWindow(), {
        title: 'Open .iconset',
        filters: [{
            name: 'Icon Set',
            extensions: ['iconset']
        }],
        properties: ['openFile']
    });

    if (!result.canceled) {
        const path = result.filePaths[0];
        await importIconSetFolder(path);
    }
}

async function importIconSetFolder(folderPath) {
    const fs = require('fs/promises');
    const path = require("path");
    let files = await fs.readdir(folderPath, {withFileTypes: true});
    files = files
        .filter(f => f.isFile() && f.name.endsWith('.png'))
        .map(f => f.name);

    for (const image of imagePlaceholders) {
        const imageSizeString = sizeString(image.size, image.resolution);

        const file = files.find(f => f.toLowerCase().endsWith(`${imageSizeString}.png`))
        if (file) {
            image.image = path.join(folderPath, file);
        }
    }

    checkState();
}

async function exportIcon() {
    const preset = currentPreset();

    const files = {};
    for (const image of imagePlaceholders) {
        const imageSizeString = sizeString(image.size, image.resolution);
        files[imageSizeString] = image.image;
    }

    let buffer;
    if (preset.output.format === 'icns') {
        buffer = await BuildIcns(files);
    } else if (preset.output.format === 'ico') {
        buffer = await BuildIco(files);
    }
    if (buffer) {
        const result = await dialog.showSaveDialog(getCurrentWindow(), {
            filters: [{
                name: 'Icon',
                extensions: [preset.output.format]
            }],
            properties: ['createDirectory']
        });

        if (!result.canceled) {
            const fs = require('fs/promises');

            await fs.writeFile(result.filePath, buffer);
        }
    }
}

function clearView() {
    presetSelector.value = '';
}

function checkState() {
    let anyImageSet = imagePlaceholders.length > 0;
    for (const image of imagePlaceholders) {
        if (image.error || !image.image) {
            anyImageSet = false;
            break;
        }
    }
    exportButton.disabled = !anyImageSet;
    clearButton.disabled = !presetSelector.value;
    importIconSetButton.disabled = !presetSelector.value;
    fileTypeLabel.innerText = currentPreset()?.output.format || '';
}

function sizeString(size, res) {
    if (res === 1) {
        return `${size}x${size}`;
    } else {
        return `${size}x${size}@${res}x`;
    }
}

function currentPreset() {
    const key = presetSelector.value;
    if (key) {
        return presets[key];
    }
}

function openInfoDialog() {
    ipcRenderer.send('open-dialog', {dialog: 'info'});
}
