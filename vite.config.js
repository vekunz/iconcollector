/**
 * @type {import('vite').UserConfig}
 */
const config = {
    root: './src',
    publicDir: './electron',
    base: '',
    build: {
        emptyOutDir: true,
        outDir: '../build',

        rollupOptions: {
            input: {
                main: './pages/main.html',
                info: './pages/info.html'
            }
        }
    }
};
export default config;
